# kookoo.py

## Audience:

This is for you if you wish to make an application based on
[kookoo.in]("http://www.kookoo.in). Currently the API is a subset of full access
provided by kookoo, it only leverages the parts of the API that you can use with
a free developer account.

## License:

BSD 2 clause License.