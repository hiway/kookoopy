from kookoo import *

class RequestHandler(KookooRequestHandler):
    response = KookooResponse()
    
    def new_call(self, caller):
        r = self.response
        return r.response(r.get_input(r.say('Enter your age: ')))
    
    def got_input(self, data):
        r = self.response
        text = 'Hi! You are %s years old!' %data
        return r.response(r.say(text + r.hangup()))

    def got_voice(self, data):
        return self.session.data



if __name__ == "__main__":
    req = RequestHandler()
    
    print req.process(sid=1223, cid=9850885021, event='NewCall')
    user_input = ''
    sid = '123'
    expecting_voice = False
    
    while user_input is not 'q':
        user_input = raw_input('> ')
        
        if user_input == 'q':
            print req.process(sid=sid, event='Disconnect')
            break

        if user_input == 'r':
            if expecting_voice is True:
                print req.process(sid=sid, event='Record', data='http://example.com/foobar.wav')
            else:
                print "Not expecting voice."
            
        try:
            if user_input[-1] == '#':
                user_input = user_input[:-1]
                
            numbers = int(user_input)
            response = req.process(sid=sid, event='GotDTMF', data=user_input)
            print response
        except:
            session = KookooSession(sid, {})
            response = ''
            pass
        
        if response is not None:
            if '<hangup></hangup>' in response:
                print 
                print "="*10 + " hanging up " + "="*10
                break
        else:
            session = KookooSession(sid, {})
            print "Invalid response. Reseting and hanging up."
            break