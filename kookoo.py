"""API to build app for kookoo.in

Copyright: (c) 2012 Harshad Sharma, All Rights Reserved.
"""

import json
import string

class KookooSession:
    id = None
    data = {}
    
    def __init__(self, id=None, data=None):
        if id is None:
            # No point going further, initiate an empty object.
            return
        
        if data is None:
            # Assume we're handling a get
            self.id = id
            self.data = self.get()
        
        else:
            self.id = id
            self.data = data
            self.set(data)


    def get(self):
        """Gets json stored in a text file, returns data structures.
        Override this function with your own session handler, memcache/db."""
        id = self.id
        
        try:
            ifile = open("%s.session" %(id), "r+")
        except:
            self.set({})
        finally:
            ifile = open("%s.session" %(id), "r+")
            
        try:
            data = json.load(ifile)
        except ValueError:
            data = {}
        
        ifile.close()
        return data
    
    
    def set(self, data):
        """Saves data as json into a text file.
        Override this."""
        id = self.id
        
        ofile = open("%s.session" %(id), "w+")
        if ofile is None:
            return None
        
        json.dump(data, ofile)
        ofile.close()
        return True
    
    def save(self):
        """Saves current data."""
        return self.set(self.data)
        
    

class KookooRequestHandler:
    session = None
    sesion_class = None
    
    def __init__(self, session_class=KookooSession):
        self.session_class = session_class 
    
    def process(self, sid=None, cid=None, event=None, data=None, called_number=None):
        """Takes request from kookoo server and processeses it"""
        self.session = self.session_class(sid)

        if self.session.data == None:
            return "Error: unable to create session!"
        
        map_event_to_function = {
            "NewCall":self._new_call,
            "Record":self._got_voice,
            "GotDTMF":self._got_input,
            "Hangup":self._hangup,
            "Disconnect":self._disconnect,
        }
        # Call the right function
        return map_event_to_function[event]( sid, cid, data, called_number)
                
    def _new_call(self, sid, cid, data, called_number):
        """Called when request for new call comes in.
        Data will be None."""
        self.session.data.update({
                            "caller":cid,
                            "session":sid,
                            "data":data,
                            "called_number":called_number
                        })
        self.session.save()        
        return self.new_call(cid)
        
    def new_call(self, cid):
        pass
    
    
    def _got_voice(self, sid, cid, data, called_number):
        """Called when kookoo finishes recording to a file.
        Data will be URL pointing to .wav file with recorded voice."""
        self.session.data.update({'data':data})
        self.session.save()
        return self.got_voice(data)
        
    def got_voice(self, data):
        pass
    
    
    def _got_input(self, sid, cid, data, called_number):
        """Called when kookoo finishes receiving user input.
        Data will be string of digits entered by user."""
        self.session.data.update({'data':data})
        self.session.save()
        return self.got_input(data)
        
    def got_input(self, data):
        pass
    
    
    def _hangup(self, sid, cid, data, called_number):
        """Called when caller hangs up current call.
        Data will be None."""        
        return self.hangup()
    
    def hangup(self):
        pass

    
    def _disconnect(self, sid, cid, data, called_number):
        """Called after kookoo disconnects caller (via our previous command).
        Data will be None."""
        return self.disconnect()
    
    def disconnect(self):
        pass
        


class KookooResponse:
    wrapper_template = string.Template("""<?xml version="1.0" encoding="UTF-8"?>
    <Response>
        $response
    </Response>""")
    
    template = string.Template(
        "<$response_type$attributes>$content</$response_type>")
    
    def response(self, response):
        return self.wrapper_template.substitute(response=response)
    
    def say(self, text):
        return self.template.substitute(
            response_type = 'playtext',
            attributes = '',
            content = text,
        )

    def hangup(self):
        return self.template.substitute(
            response_type = 'hangup',
            attributes = '',
            content = '',
        )
        
    def play(self, url):
        return self.template.substitute(
            response_type = 'playaudio',
            attributes = '',
            content = url,
        )
        
    def get_input(self, prompt, length=4, terminate='#', timeout=5):
        attributes = """ l="%s" t="%s" o="%s" """ %(
            length, terminate, timeout * 1000,
        )
        return self.template.substitute(
            response_type = 'collectdtmf',
            attributes = attributes,
            content = prompt,
        )

    def get_voice(self, filename, format='wav', silence=3, max_duration=30):
        attributes = """ format="%s" silence="%s" maxduration="%s" """ %(
            format, silence, max_duration,
        )

        return self.template.substitute(
            response_type = 'record',
            attributes = attributes,
            content = filename,
        )


